import { Test } from "tape"
import Maybe from "../src/maybe"
import { compose } from "../src/utils"
import { curry2 } from "../src/utils"

const test = require("tape")

test("Create container with Maybe.of and extract with getUnsafe()", (t: Test) => {
  let maybe: Maybe<undefined|number> = Maybe.of(undefined)
  t.throws(maybe.getUnsafe, "getUnsafe() throws when value is Nothing")
  maybe = Maybe.of(1)
  t.equal(maybe.getUnsafe(), 1, "getUnsafe with Just value does not throw")
  t.end()
})

test("Create container with Maybe.of and extract with getOrElse", (t: Test) => {
  let maybe: Maybe<undefined|number> = Maybe.of(undefined)
  t.equal(maybe.getOrElse(1), 1, "getOrElse(default) returns default when value is Nothing")
  maybe = Maybe.of(0)
  t.equal(maybe.getOrElse(1), 0, "getOrElse(deafult) with Just value returns value")
  t.end()
})

test("property .isJust", (t: Test) => {
  t.true(Maybe.of("Wyatt").isJust, "String value is JUST")
  t.true(Maybe.of({ name: "Wyatt"}).isJust, "Object value is JUST")
  t.true(Maybe.of(["Wyatt"]).isJust, "Array value is JUST")
  /* tslint:disable */
  // @ts-ignore: no-null-keyword
  let maybe = Maybe.of(null)
  /* tslint:enable */
  t.false(maybe.isJust, "null value is NOT Just")
  t.false(Maybe.of(undefined).isJust, "undefined value is NOT Just")
  t.end()
})

test("property .isNothing", (t: Test) => {
  t.true(Maybe.of(undefined).isNothing, "undefined value is Nothing")
  /* tslint:disable */
  // @ts-ignore: no-null-keyword
  let maybe = Maybe.of(null)
  /* tslint:enable */
  t.true(maybe.isNothing, "null value is Nothing")
  t.end()
})

test("Maybe.nothing()", (t: Test) => {
  t.false(Maybe.nothing().isJust)
  t.true(Maybe.nothing().isNothing)
  t.end()
})

test("Maybe.just()", (t: Test) => {
  t.false(Maybe.just(1).isNothing)
  t.true(Maybe.just(0).isJust)
  t.end()
})

test("Maybe.of(x?).map(fn)", (t: Test) => {
  let cowboy = Maybe.of({name: "Wyatt"})
  let deputy = cowboy.map((v: {name: string}) => { return {name: `${v.name} Earp`} })
  let expected = deputy.getUnsafe().name
  t.equal(expected, "Wyatt Earp", `'${expected}' equals 'Wyatt Earp'`)
  t.end()
})

test("Maybe is a Pointed Functor with identity", (t: Test) => {
  // Preserves identity `object.map(x => x) ≍ object`
  let id: Maybe<any> = Maybe.of("ID")
  t.deepEqual(id.map((i: string) => i), id, "Preserves identity of a string")
  id = Maybe.of({ id: "ID" })
  t.deepEqual(id.map((i: {id: string}) => i), id, "Preserves identity of object")
  t.end()
})

test("Maybe is a Pointed Functor, composable", (t: Test) => {
  // Composable `object.map(compose(f, g)) ≍ object.map(g).map(f)`
  let maybe: Maybe<number> = Maybe.of(1)
  let add1 = (n: number): number => n + 1
  let add2 = (n: number): number => n + 2
  let result = maybe.map(compose(add2, add1))
  t.equal(result.getUnsafe(), 4)

  result = maybe.map(add1).map(add2)
  t.equal(result.getUnsafe(), 4)
  t.end()
})

test("join() extracts nested Maybe<Maybe<T>> as Maybe<T>", (t: Test) => {
  let cowboy = Maybe.of("Wyatt")
  let deputy = Maybe.of(cowboy)
  let extracted = deputy.join()
  let expected = extracted.getUnsafe()
  t.equal(expected, "Wyatt", `extracted Maybe['${expected}'] equals 'Wyatt'`)
  t.end()
})

test("chain(fn) to sequence functions that return Maybe", (t: Test) => {
  type Item = { price: number | undefined }
  type Cart = { items: Array<Item> }

  let prop = (k: string, obj: {[prop: string]: any}) => Maybe.of(obj[k])
  let price = curry2(prop)("price")
  let items = curry2(prop)("items")
  let cartItem = (cart: Cart, i: number): Maybe<Item> => items(cart).map((items: Array<Item>): Maybe<Item> => Maybe.of(items[i])).join()

  let cart: Cart = { items: [{ price: undefined }] }

  let getItem: (i: number) => Maybe<Item> = curry2(cartItem)(cart)
  let expected = getItem(0).chain(price)
  t.equal(expected.getOrElse(undefined), undefined)

  cart = { items: [{ price: 9.99 }] }

  getItem = curry2(cartItem)(cart)
  expected = getItem(0).chain(price)
  t.equal(expected.getOrElse(undefined), 9.99)
  t.end()
})

test(".ap(M) Apply a function in functor to a value in another functor of the same type",  (t: Test) => {
  // F.of(x).map(fn) == F.of(fn).ap(F.of(x))
  let add: (n: number) => (n: number) => number =
    curry2((x: number, y: number): number => x + y)
  let result = Maybe.of(add(2)).ap(Maybe.of(3))
  t.equal(result.getUnsafe(), 5)
  result = Maybe.of(2).map(add).ap(Maybe.of(3))
  t.equal(result.getUnsafe(), 5)
  t.end()
})

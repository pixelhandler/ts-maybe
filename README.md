# TS-Maybe

An option type `Maybe<Just|Nothing>` in TypeScript

Why Maybe ?
- https://oliverjash.me/2017/04/10/why-use-a-maybe-type-in-javascript

What is a Maybe?
- `Maybe<Just|Nothing>` provides safe handling of null|undefined or any value
- A Maybe implements Pointed Functor, an Applicative, and Monad interfaces
- Also known as a nullable type as `Option<Some|None>`

Interfaces and Maybe type inspired by:
- Maybe-not https://github.com/danielchappell/maybe-not
- True Myth https://true-myth.js.org

Functional programming resources:
- https://github.com/hemanth/functional-programming-jargon
- https://drboolean.gitbooks.io/mostly-adequate-guide/content/
- http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html

## Development

Use LTS version of [Node.js](https://nodejs.org/)

- Download and install for your operating system
- Perhaps use [Node Version Manager](https://github.com/creationix/nvm)
  - `nvm install --lts`

Clone the repo then, within the project root, run:

- `npm install`

## Testing

This boilerplate setup uses [TypeScript](https://www.typescriptlang.org/docs/home.html)
and [Tape](https://github.com/substack/tape) as a test runner.

Use node scripts in package.json file

- Check syntax `npm run lint`
- Run tests `npm run test`
- Watch changes and run tests `npm run watch`

/*
  An object that implements a `map` function which, while running over each
  value in the object to produce a new object, adheres to two rules:
  - Preserves identity `object.map(x => x) ≍ object`
  - Composable `object.map(compose(f, g)) ≍ object.map(g).map(f)`

  See https://github.com/hemanth/functional-programming-jargon#functor
*/
export interface Functor<T> {
  map<U>(fn: (value: T) => U): Functor<U>
}

/*
  Pointed Functor has an `of` function which puts any single value into it.
  - `of` is also known as return in functional languages
*/
export interface Pointed<T> extends Functor<T> {
  // Container maker `Pointed.of()` puts any single value in, a Pointed Functor
  // static of<A>(value: A): Pointed<A> {
  //   return new Pointed(value)
  // }

  // Apply function to container value, Functor
  map<U>(fn: (value: T) => U): Pointed<U>
}

/*
  An applicative functor is an object with an `ap` function. `ap` applies a function
  in the object to a value in another object of the same type.

  - `ap` is a function that can apply the function contents of one functor to the
    value contents of another.
  - F.of(x).map(fn) == F.of(fn).ap(F.of(x))

  See https://github.com/hemanth/functional-programming-jargon#applicative-functor
*/
export interface Applicative<T> extends Pointed<T> {
  ap<U>(m: Pointed<T>): Pointed<U>
}

/*
  Monads are pointed functors that can flatten.

  A monad is an object with of and chain functions. `chain` is like `map` except it
  un-nests the resulting nested object. `chain` is also known as `flatmap` and `bind`.

  See https://github.com/hemanth/functional-programming-jargon#monad
*/
export interface Monad<T> extends Pointed<T> {
  join<T>(): Maybe<T>
  chain<A>(fn: (arg: T) => Maybe<A>): Maybe<A>
}

export type Just = any // with --strictNullChecks does not include null | undefined
export type Nothing = null | undefined
type Fn = (value: Just) => Just

/*
  Maybe is an Option Type, a popular abstraction for defining values that may
  or may not exist.

  - `map` over the Maybe to use the container's value
  - Provide a default when finally reading the inner value with `getOrElse`.

  See https://github.com/hemanth/functional-programming-jargon#option
*/
export default class Maybe<T> implements Monad<T>, Applicative<T> {
  constructor(private value?: T | Nothing | Fn | Maybe<T> | Maybe<Maybe<T>>) {}

  // Maybe.just() is just (always) something <T> and never nothing (undefined)
  static just<Just>(value: Just) {
    if (value === undefined || value === null) {
      throw new TypeError("Requires non null|undefined value")
    }
    return new Maybe(value)
  }

  // Maybe.nothing() is always undefined never Just something
  static nothing<Nothing>(): Maybe<Nothing> {
    return new Maybe<Nothing>(undefined)
  }

  // Container maker `Mabye.of()` puts any single value in
  static of<A>(value: A): Maybe<A> {
    if (value === undefined || value === null) {
      return Maybe.nothing<A>()
    }
    return Maybe.just<Just>(value)
  }

  // Apply function to container value
  map<U>(fn: (value: T) => U): Maybe<U> {
    if (this.value !== undefined) {
      return Maybe.of(fn(this.value as T))
    }
    return Maybe.nothing<U>()
  }

  // Apply a function in the object to a value in another object of the same type
  // - F.of(x).map(fn) == F.of(fn).ap(F.of(x))
  ap<T, U>(m: Maybe<T>): Maybe<U> {
    return this.isNothing ? Maybe.nothing<U>() : m.map(<(v: T) => U>this.value)
  }

  // `chain` uses `map` and `join` to return another Functor, a.k.a. `flatmap` | `bind`
  // Use to sequence functions that return Maybe
  chain<A>(fn: (arg: T) => Maybe<A>): Maybe<A> {
    return this.map(fn).join() as Maybe<A>
  }

  // Extract nested Maybe
  join<T>(): Maybe<T> {
    return this.isNothing ? Maybe.nothing<T>() : (this.value as Just)
  }

  // Safely extract container value, with a fallback value
  getOrElse(fallback: T): T {
    return this.value !== undefined ? (this.value as T) : fallback
  }

  // Get value or throw error
  getUnsafe(): Just | Error {
    if (this.isNothing) {
      throw new Error("is nothing")
    }
    return this.value
  }

  // is container value `undefined` ?
  get isNothing(): boolean {
    return (this.value as Nothing) === undefined
  }

  // is container value something <T> ?
  get isJust(): boolean {
    return (this.value as Just) !== undefined
  }
}

// Utils

/*
  Function Composistion
  see https://github.com/hemanth/functional-programming-jargon#function-composition
*/
export type anyFn<T> = (...args: any[]) => T
export const compose: (f: anyFn<any>, g: anyFn<any>) => any = function(f: anyFn<any>, g: anyFn<any>): any {
  return function(x: any) {
    return f(g(x))
  }
}

// See https://gist.github.com/donnut/fd56232da58d25ceef1#gistcomment-1617985
export interface CurriedFunction2<T1, T2, R> {
  (t1: T1): (t2: T2) => R
  (t1: T1, t2: T2): R
}

export function curry2<T1, T2, R>(f: (t1: T1, t2: T2) => R): CurriedFunction2<T1, T2, R> {
  function curriedFunction(t1: T1): (t2: T2) => R
  function curriedFunction(t1: T1, t2: T2): R
  function curriedFunction(t1: T1, t2?: T2): any {
    switch (arguments.length) {
      case 1:
        return function(t2: T2): R {
          return f(t1, t2)
        }
      case 2:
        return f(t1, t2 as T2)
    }
  }
  return curriedFunction
}
